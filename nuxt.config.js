export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'Waka FM',
    description: 'Waka FM',
    htmlAttrs: {
      lang: 'vi'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1, maximum-scale=1' },
      { hid: 'description', name: 'description', content: 'Waka FM' },
      { name: 'format-detection', content: 'telephone=no' },
      {
        hid: 'og:image',
        property: 'og:image',
        content: 'https://fm.waka.vn/images/bg-banner-home.png'
      },
      {
        hid: 'og:title',
        name: 'og:title',
        content: 'Waka FM'
      },
      {
        hid: 'og:description',
        name: 'og:description',
        content: 'Waka FM'
      }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: 'https://fm.waka.vn/favicon.ico' }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '@/assets/css/main.css',
    '@/assets/scss/main.scss',
    'element-ui/lib/theme-chalk/index.css'
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    '@/plugins/fetcher.js',
    {
      src: '~/plugins/element-ui.js',
      ssr: true
    }
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module',
    // https://go.nuxtjs.dev/tailwindcss
    '@nuxtjs/tailwindcss',
    '@nuxt/postcss8',
    '@nuxtjs/device',
    '@nuxtjs/moment'
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    // https://go.nuxtjs.dev/pwa
    '@nuxtjs/pwa',
    '@nuxtjs/auth-next',
    ['nuxt-lazy-load', {
      images: true,
      videos: true,
      directiveOnly: true,
      defaultImage: '/svgs/thumb-loading-vertical.svg'
    }]
    // '@nuxt/http'
  ],

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    // Workaround to avoid enforcing hard-coded localhost:3000: https://github.com/nuxt-community/axios-module/issues/308
    baseURL: process.env.API_URL,
    proxy: true
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    postcss: {
      plugins: {
        tailwindcss: {},
        autoprefixer: {},
      },
    }
  },

  moment: {
    locales: ['vi']
  },

  device: {
    refreshOnResize: true
  },

  env: {
    BASE_URL: process.env.BASE_URL || 'http://localhost:3000',
    API_URL: process.env.API_URL || 'https://beta-api.waka.vn',
    IS_DEV: process.env.IS_DEV || 0
  },

  auth: {
    plugins: [
      '~/plugins/fetcher.js',
      '~/plugins/mixins.js',
    ],
    redirect: {
      login: '/login',
      logout: '/',
      home: false,
      callback: '/login'
    },
    cookie: {
      options: {
        expires: 30,
        maxAge: 60 * 60 * 24 * 30
      }
    },
    strategies: {
      local: {
        token: {
          global: false,
          type: '',
          maxAge: 60 * 60 * 24 * 30
        },
        user: {
          autoFetch: true
        },
        endpoints: {
          user: false,
          login: false,
          logout: false
        }
      },
      google: {
        clientId: process.env.GOOGLE_CLIENT_ID || '508516412404-q9tig8bkpjt7hsjqeq3l5g2mmt17hgla.apps.googleusercontent.com',
        codeChallengeMethod: '',
        responseType: 'token id_token',
        endpoints: {
          token: false,
          userInfo: false
        },
        scope: ['profile', 'email'],
        token: {
          global: false,
          type: '',
          maxAge: 60 * 60 * 24 * 30
        },
        redirectUri: `${process.env.BASE_URL}/social-callback`,
        logoutRedirectUri: undefined
      },
      facebook: {
        endpoints: {
          token: false,
          userInfo: false
        },
        token: {
          global: false,
          type: '',
          maxAge: 60 * 60 * 24 * 30
        },
        clientId: process.env.FACEBOOK_CLIENT_ID || '247115549152339',
        scope: ['public_profile', 'email'],
        redirectUri: `${process.env.BASE_URL}/social-callback`,
        logoutRedirectUri: undefined
      }
    }
  }
}
