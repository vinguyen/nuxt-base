export const listApi = {
  LIST_TOPIC_HOME: 'LIST_TOPIC_HOME',
}

export const urlRequest = {
  LIST_TOPIC_HOME: '/fm/listTopicHome'
}

export const paramsSecures = {
  LIST_TOPIC_HOME: [
    'account',
    'id',
    'os'
  ]
}
