export const routeNames = {
  HOME: 'index'
}

export const keyStores = {
  accountInfo: 'fm.accountInfo',
  accessToken: 'fm.auth.accessToken',
  refreshToken: 'fm.auth.refreshToken',
  idsIgnoreFavorite: 'fm.auth.idsIgnoreFavorite',
  redirectAfterLogin: 'fm.auth.redirectAfterLogin'
}
