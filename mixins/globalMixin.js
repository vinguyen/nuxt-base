export default {
  //
  computed: {
    isAuthenticate () {
      return this.$auth.loggedIn
    },
    accountInfo () {
      return this.$auth.user
    }
  },
  methods: {
    scrollToTop () {
      window.scrollTo({
        top: 0,
        left: 0,
        behavior: 'smooth'
      })
    }
  }
}
