export default {
  //
  CHANGE_DEVICE (state, value) {
    state.isDeviceWeb = value
  },
  CHANGE_ACCOUNT_INFO (state, accountInfo) {
    if (accountInfo) {
      state.account = accountInfo.userId
      state.tid = accountInfo.tid
    } else {
      state.account = ''
      state.tid = ''
    }
  },
  CHANGE_DEVICE_ID (state, value) {
    state.deviceId = value
  },
  CHANGE_PROPERTIES (state, payload) {
    Object.entries(payload).forEach(([key, value]) => {
      state[key] = value
    })
  }
}
