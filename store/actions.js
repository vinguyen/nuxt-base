import { genDeviceId } from '~/helpers/common'
import { keyStores } from '~/configs/common'

export default {
  //
  async nuxtServerInit ({
    dispatch,
    commit,
    state
  }, {
    route,
    $device,
    $fetcher,
    redirect
  }) {
    if ($device.isMobileOrTablet) {
      commit('CHANGE_DEVICE', false)
    } else {
      commit('CHANGE_DEVICE', true)
    }
    const deviceId = this.$auth.$storage.getCookie('device_id')
    if (deviceId) {
      commit('CHANGE_DEVICE_ID', deviceId)
    } else {
      const newDeviceId = genDeviceId()
      this.$auth.$storage.setCookie('device_id', newDeviceId)
      commit('CHANGE_DEVICE_ID', newDeviceId)
    }
  },
  changeProperties ({ commit }, payload) {
    commit('CHANGE_PROPERTIES', payload)
  },
  changeDevice ({ commit }, value) {
    commit('CHANGE_DEVICE', value)
  },
  async onLogout ({
    dispatch,
    commit
  }) {
    await this.$auth.logout()
    await this.$auth.strategy.token.reset()
    this.$auth.$storage.removeCookie(keyStores.accountInfo, true)
    this.$auth.$storage.removeCookie(keyStores.refreshToken, true)
  }
}
