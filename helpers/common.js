import slugify from 'slugify'
import { MD5 } from '~/helpers/hash'

export function createSlug (str) {
  str = str.replace(/<[^>]*>/g, '')
  return slugify(str, {
    replacement: '-',
    remove: /[*+~.()'"!:@]/g,
    lower: true,
    strict: true,
    locale: 'vi',
    trim: true
  })
}

export function randomInit (min, max) {
  min = Math.ceil(min)
  max = Math.floor(max)
  return Math.floor(Math.random() * (max - min) + min)
}

export function genDeviceId () {
  return MD5(`${Date.now()}${randomInit(1000, 9999)}`)
}

export function generateAvatar (text, foregroundColor, backgroundColor) {
  const canvas = document.createElement('canvas')
  const context = canvas.getContext('2d')

  canvas.width = 200
  canvas.height = 200

  // Draw background
  context.fillStyle = backgroundColor
  context.fillRect(0, 0, canvas.width, canvas.height)

  // Draw text
  context.font = 'Bold 80px Arial'
  context.fillStyle = foregroundColor
  context.textAlign = 'center'
  context.textBaseline = 'middle'
  context.fillText(text, canvas.width / 2, canvas.height / 1.9)

  return canvas.toDataURL('image/png')
}
