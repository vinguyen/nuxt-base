import md5 from 'crypto-js/md5'
import CryptoJS from 'crypto-js'
import { paramsSecures } from '~/configs/api/listApi'

export function MD5 (str) {
  return md5(str).toString()
}

export function createSign (url, params = {}, token) {
  let paramsString = ''

  paramsSecures[url].forEach((key) => {
    paramsString += (` ${encodeURIComponent(params[key])}`)
  })
  return CryptoJS.enc.Base64.stringify(CryptoJS.HmacSHA1(paramsString.trim(), token))
}
