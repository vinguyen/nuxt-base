import { listApi, urlRequest } from '~/configs/api/listApi'
import { createSign, MD5 } from '~/helpers/hash'
import { authTypes } from '~/configs/common'

export default function ({ $axios, store, $auth }, inject) {
  const options = {
    baseURL: process.env.API_URL,
    credentials: true
  }

  // Create a custom axios instance
  const fetcher = $axios.create(options)
  // Add a request interceptor
  fetcher.interceptors.request.use(
    (requestConfig) => {
      // const token = store.state.authClient.access_token
      // if (token) {
      //   requestConfig.headers.Authorization = `Bearer ${token}`
      // }
      return requestConfig
    },
    (requestError) => {
      // eslint-disable-next-line no-undef
      Raven.captureException(requestError)

      return Promise.reject(requestError)
    }
  )

  // Add a response interceptor
  fetcher.interceptors.response.use(
    response => response,
    (error) => {
      if (error.response && error.response.status >= 500) {
        // eslint-disable-next-line no-undef
        Raven.captureException(error)
      }

      return Promise.reject(error)
    }
  )
  fetcher.api = listApi
  fetcher.getResponse = (result, key) => {
    let res = false
    const response = result.data
    if (parseInt(response.code) === 200) {
      res =
        typeof response.data[key] !== 'undefined'
          ? response.data[key]
          : null
    }
    return res
  }

  fetcher.allResponse = (result) => {
    const response = result.data
    if (parseInt(response.code) === 0) {
      if (response.data) {
        return response.data
      }
      return []
    }
    if (parseInt(response.code) === 1) {
      return []
    }
    // if (parseInt(response.code) === 101) {
    //   return 403
    // }
    return false
  }

  fetcher.allResponseDetail = (result) => {
    const response = result.data
    if (parseInt(response.code) === 0) {
      if (response.data && response.data.id) {
        return response.data
      }
      return false
    }
    return false
  }

  fetcher.successNotify = (result) => {
    const response = result.data
    if (parseInt(response.code) === 200) {
      // return response
    } else {
      // return response.message
    }
  }

  fetcher.errorNotify = (error) => {
    const errors =
      error.response && error.response.data && error.response.data.errors
        ? error.response.data.errors
        : []

    for (const i in errors) {
      // eslint-disable-next-line no-unreachable-loop
      for (let a = 0; a < errors[i].length; a++) {
        // Notify.error(errors[i][a])
        // return
      }
    }

    if (!errors.length && error.message) {
      // Notify.error(error.message)
    }
  }

  fetcher.getError = (error) => {
    const errors =
      error.response && error.response.data && error.response.data.errors
        ? error.response.data.errors
        : []

    for (const i in errors) {
      // eslint-disable-next-line no-unreachable-loop
      for (let a = 0; a < errors[i].length; a++) {
        return errors[i][a]
      }
    }

    if (!errors.length && error.message) {
      return error.message
    }
  }

  fetcher.postManual = (url, data = {}, conf = {}, dataNoEncrypt = {}) => {
    const account = store.state.account ? store.state.account : 'guest'
    const tokenMd5 = data.account ? data.account : 'guest'
    data = Object.assign({}, {
      os: 'web',
      id: store.state.deviceId,
      account
    }, data)

    const token = store.state.tid ? store.state.tid : MD5(tokenMd5)

    const secureCode = createSign(url, data, token)

    data = {
      ...data,
      secure_code: secureCode
    }
    let formData
    if (typeof FormData !== 'undefined') {
      formData = new FormData()
    } else {
      formData = new URLSearchParams()
    }
    Object.entries(data).forEach(([key, value]) => {
      formData.append(key, value)
    })

    return fetcher.post(urlRequest[url], formData, conf)
      .then((response) => {
        return Promise.resolve(response)
      })
      .catch((error) => {
        // eslint-disable-next-line no-console
        console.log('error', error)
        return Promise.reject(error)
      })
  }

  fetcher.getManual = (url, data = {}, conf = {}, dataNoEncrypt = {}) => {
    const account = store.state.account ? store.state.account : 'guest'
    const tokenMd5 = data.account ? data.account : 'guest'
    data = Object.assign({}, {
      os: 'web',
      id: store.state.deviceId,
      account
    }, data)

    const token = store.state.tid ? store.state.tid : MD5(tokenMd5)

    const secureCode = createSign(url, data, token)

    data = {
      ...data,
      secure_code: secureCode
    }

    return fetcher.get(urlRequest[url], {
      params: {
        ...data,
        secure_code: secureCode
      }
    }, conf).then((response) => {
      return Promise.resolve(response)
    }).catch((error) => {
      // eslint-disable-next-line no-console
      console.log('error', error)
      return Promise.reject(error)
    })
  }

  // Inject to context as $fetcher
  inject('fetcher', fetcher)
}
