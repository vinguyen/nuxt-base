import Vue from 'vue'

import {
  Popover,
  Loading,
  Notification,
} from 'element-ui'

import lang from 'element-ui/lib/locale/lang/vi'
import locale from 'element-ui/lib/locale'
// configure language
locale.use(lang)
Vue.component(Popover.name, Popover)
Vue.use(Loading.directive)
Vue.prototype.$loading = Loading.service
Vue.prototype.$notify = Notification
